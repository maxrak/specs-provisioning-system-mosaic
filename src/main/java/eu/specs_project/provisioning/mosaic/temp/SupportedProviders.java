package eu.specs_project.provisioning.mosaic.temp;

public enum SupportedProviders {
	AWS_EC2, GO2CLOUD, GOGRID, HP, RACKSPACE, TRMK, V_CLOUD;
	
	
	public String toString(){
		 String s = super.toString();
		 String toReturn = s.replace("_", "-");
		 return toReturn.toLowerCase();
	}
	
}
