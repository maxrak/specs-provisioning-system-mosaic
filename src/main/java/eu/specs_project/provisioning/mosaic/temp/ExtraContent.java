package eu.specs_project.provisioning.mosaic.temp;

import eu.specs_project.provisioning.mosaic.failure_recovery.PolicyParameters;
import eu.specs_project.provisioning.mosaic.mpi.MPIInfo;

public class ExtraContent {

	private MPIInfo info;
	private PolicyParameters policy;
	
	public ExtraContent(){}
	
	public ExtraContent(MPIInfo info, PolicyParameters policy) {
		super();
		this.info = info;
		this.policy = policy;
	}

	public MPIInfo getInfo() {
		return info;
	}

	public void setInfo(MPIInfo info) {
		this.info = info;
	}

	public PolicyParameters getPolicy() {
		return policy;
	}

	public void setPolicy(PolicyParameters policy) {
		this.policy = policy;
	}
	
}
