package eu.specs_project.provisioning.mosaic.temp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jclouds.compute.RunNodesException;

import eu.specs_project.provisioning.mosaic.failure_recovery.RecoveryPolicy;
import eu.specs_project.provisioning.mosaic.failure_recovery.RecoveryResult;
import eu.specs_project.provisioning.mosaic.failure_recovery.RecoveryResult.RecoveryOutcome;
import eu.specs_project.provisioning.mosaic.temp.CloudService.NodesInfo;
import eu.specs_project.provisioning.mosaic.temp.ScriptExecutionResult.ScriptExecutionOutcome;

public class Cluster {
	
	private final UUID id;
	private String clusterName, clusterDescription, defaultUser, privateKeyDefaultUser, image, hwId, 
		provider, accessKey, secretKey, publicKeyNewUser, privateKeyNewUser, errorDescription;
	private ClusterNode masterNode;
	private UUID user;
	private List<ClusterNode> slaveNodes;
	private int numberOfInstances;
	private Date creationDate, lastModifyDate;
	private State clusterState;
	

	public Cluster(String id, String clusterName, String provider, String defaultUser, int numberOfNodes, String accessKey, String secretKey,
			String clusterDescr, String image, String hwId, String userId){
		this.id = UUID.fromString(id);
		this.clusterName=clusterName;
		this.image = image;
		this.hwId = hwId;
		clusterDescription = clusterDescr;
		numberOfInstances = numberOfNodes;
		this.creationDate = new Date(System.currentTimeMillis());
		this.defaultUser=defaultUser;
		this.provider=provider;
		this.accessKey=accessKey;
		this.secretKey=secretKey;
		user=UUID.fromString(userId);
		clusterState = State.CREATING;
	}

	
	public void createNodesInCluster() throws RunNodesException{
		initCluster(clusterName, numberOfInstances, image, hwId);
		lastModifyDate = new Date(System.currentTimeMillis());
	}
	
	public void wget(String url){
		
	}
	
	public Map<String, List<String>> executeScriptAsRoot(TargetScriptExecution nodes, String[] instructions, RecoveryPolicy policy){
		return executeScript(defaultUser, privateKeyDefaultUser, nodes, instructions, true, policy);
	}
	
	public Map<String, List<String>> executeScriptAsUser(TargetScriptExecution nodes, String[] instructions, String user, String privateKey, RecoveryPolicy policy){
		return executeScript(user, privateKey, nodes, instructions, false, policy);
	}
	
	public boolean destroyCluster(){
		CloudService cs = new CloudService(provider, defaultUser, accessKey, secretKey);
		if(clusterState==State.READY){
			clusterState=State.DESTROYING;
			slaveNodes.add(masterNode);
			cs.destroyCluster(slaveNodes);
			lastModifyDate = new Date(System.currentTimeMillis());
			clusterState=State.DESTROYED;
			return true;
		}
		return false;
		
	}
		
	public boolean startCluster(){
		CloudService cs = new CloudService(provider, defaultUser, accessKey, secretKey);
		if(clusterState==State.STOPPED){
			clusterState=State.CREATING;
			List<ClusterNode> hosts = slaveNodes;
			hosts.add(masterNode);
			cs.resumeNodesInGroup(hosts);
			lastModifyDate = new Date(System.currentTimeMillis());
			clusterState=State.READY;
			return true;
		}
		return false;
	}
	
	public boolean stopCluster(){
		CloudService cs = new CloudService(provider, defaultUser, accessKey, secretKey);
		if(clusterState==State.READY){
			clusterState=State.STOPPING;
			List<ClusterNode> hosts = slaveNodes;
			hosts.add(masterNode);
			cs.suspendNodesInGroup(hosts);
			lastModifyDate = new Date(System.currentTimeMillis());
			clusterState=State.STOPPED;
			return true;
		}
		return false;
	}
	
	
/*	public int addNodes(int numberOfNodes) throws RunNodesException{
		if(clusterStatus==Status.READY){
			clusterStatus=Status.CREATING;
			int i = 0;
			NodeMetadata newNode = null;
			while(i<numberOfNodes){
				newNode = cs.addNode(clusterName);
				cs.addNewUserViaExecForSingleNode(newNode, info.mpiUserUsername, null);
				cs.executeSlaveScriptViaExecForSingleNode(newNode);
				cs.addSlaveNode(newNode);
				slaveNodes.add(newNode);
			}
			info.clusterStatus=Status.READY;
			return 0;
		}
		return 1;
	}
*/		
	
	public HashMap<String, String> createNewUser(String userName, String password, RecoveryPolicy policy) throws RunNodesException{
		CloudService cs = new CloudService(provider, defaultUser, accessKey, secretKey);
		ScriptExecutionResult result = cs.addNewUserViaExec(userName, password, masterNode, slaveNodes, privateKeyDefaultUser);
		HashMap<String, String> keys =	(HashMap<String, String>) result.getAdditionalInfo();
		int nodesWithError = 0, nodesReplaced = 0, nodesRepaired = 0, nodesDestroyed = 0;
		
		if(result.getOutcome().equals(ScriptExecutionOutcome.OK)){
			return (HashMap<String, String>) result.getAdditionalInfo();
		}
		if(result.getOutcome().equals(ScriptExecutionOutcome.MASTER_ERROR)){
			clusterState = State.ERROR;
			errorDescription = "Error on Master Node. \n"
					+ "Must destroy cluster";
					
			return null;
		}
		policy.setParameters(keys);
		nodesWithError = result.getNodesWithError().size();
		for(ClusterNode n: result.getNodesWithError()){
			RecoveryResult recResult = policy.recovery(cs, n, result);
			
			if(recResult.getOutcome().equals(RecoveryOutcome.SUCCEEDED)){
				nodesRepaired++;
				errorDescription = nodesWithError + " Slave Node/s with errors:\n"
						+ nodesRepaired +" repaired; "+ nodesDestroyed + " destroyed; "+ nodesReplaced + " replaced.";
			}
			else if(recResult.getOutcome().equals(RecoveryOutcome.DESTROYED)){
				clusterState = State.WARNING;
				nodesDestroyed++;
				errorDescription = nodesWithError + " Slave Node/s with errors:\n"
						+ nodesRepaired +" repaired; "+ nodesDestroyed + " destroyed; "+ nodesReplaced + " replaced.";
				slaveNodes.remove(n);
			}
			else if(recResult.getOutcome().equals(RecoveryOutcome.REPLACED)){
				clusterState = State.WARNING;
				nodesDestroyed++;
				nodesReplaced++;
				errorDescription = nodesWithError + " Slave Node/s with errors:\n"
						+ nodesRepaired +" repaired; "+ nodesDestroyed + " destroyed; "+ nodesReplaced + " replaced.";
				slaveNodes.remove(n);
				slaveNodes.add(recResult.getNewNode());
			}
		}		
//		FailureRecoveryExecutor recovery = new FailureRecoveryExecutor(cs, null, masterNode, slaveNodes);		
//		OperationResult opRes = recovery.executeUserInstallationWithPolicy(clusterName, userName, password, privateKeyDefaultUser);
//		if(opRes.getOutcome().equals(OperationOutcome.NO_ERROR)){
//
//		}
//		else if(opRes.getOutcome().equals(OperationOutcome.MASTER_ERROR)){
//			this.clusterState = State.ERROR;
//			this.errorDescription="Master node Error. Must destroy cluster";
//			return null;
//		}
//		else if(opRes.getOutcome().equals(OperationOutcome.SLAVE_ERROR_RESOLVED)){
//			this.clusterState = State.WARNING;
//			this.errorDescription="Slave node/s Error. Resolved";
//
//		}
//		else if(opRes.getOutcome().equals(OperationOutcome.WARNING)){
//			this.clusterState = State.WARNING;
//			this.errorDescription="Slave node/s Error. Not Resolved";
//			
//		}
//		else if(opRes.getOutcome().equals(OperationOutcome.ONE_OR_MORE_SLAVE_NODES_REPLACED)){
//			this.clusterState = State.WARNING;
//			this.errorDescription="Slave node/s Error. Replaced";
//			List<ClusterNode> nodesDeleted = opRes.getNodesDeleted();
//			List<ClusterNode> newNodes= opRes.getNewNodes();
//			
//			for(ClusterNode n : nodesDeleted){
//				slaveNodes.remove(n);
//			}
//			for(ClusterNode n : newNodes){
//				slaveNodes.add(n);
//			}
//			int numNodes = nodesToBeReplaced.size(), j=0;
//			ClusterNode n;
//			for(int i = 0; i<numNodes; i++){
//				while(j<policy.getMaxRetriesOfPolicy()){
//					n = recovery.replaceNodeWithUserInst(clusterName);
//					slaveNodes.add(n);
//				}
//			}
//			for(ClusterNode node : nodesToBeReplaced){
//				slaveNodes.remove(node);
//			}
//		}	
		
	//	ScriptExecutionResult result = cs.addNewUserViaExec(userName, password, masterNode, slaveNodes, privateKeyDefaultUser);
		return keys;
	}
	
	public String obtainMasterPrivateIP(){		
		return masterNode.getPrivateIP();
	}
	
	public void setClusterState(State clusterState) {
		this.clusterState = clusterState;
	}

	public UUID getId() {
		return id;
	}

	public String getMasterNodeId(){
		return masterNode.getId();
	}

	public State getClusterState() {
		return clusterState;
	}

	public String getPrivateKeyDefaultUser() {
		return privateKeyDefaultUser;
	}

	public List<ClusterNode> getSlaveNodes() {
		return slaveNodes;
	}

	public int getNumberOfInstances() {
		return numberOfInstances;
	}

	public ClusterNode getMasterNode() {
		return masterNode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	public static enum State{
		CREATING, READY, BUSY, STOPPING, STOPPED, DESTROYING, DESTROYED, ERROR, WARNING;
	}
	
	public enum TargetScriptExecution{
		MASTER, SLAVE, ALL;
	}
	
	private void initCluster(String clusterName, int numberOfInstances, String image, String hardwareId) throws RunNodesException{
		CloudService cs = new CloudService(provider, defaultUser, accessKey, secretKey);
		NodesInfo info = cs.createNodesInGroup(clusterName, numberOfInstances, image, hardwareId);
		ArrayList<ClusterNode> nodes = (ArrayList<ClusterNode>) info.nodes;
		masterNode = nodes.get(0);
		nodes.remove(0);
		slaveNodes = nodes;
		privateKeyDefaultUser = info.privateKey;
	}	
	
	private Map<String, List<String>> executeScript(String user, String privateKey, TargetScriptExecution nodes, String[] instructions, boolean sudo, RecoveryPolicy policy){
		CloudService cs = new CloudService(provider, defaultUser, accessKey, secretKey);
	//	policy.recovery(cs, masterNode, result);
	//	FailureRecoveryExecutor recovery = new FailureRecoveryExecutor(cs, policy, masterNode, slaveNodes);
		
		ScriptExecutionResult result;
		HashMap<String, List<String>> finalMap = new HashMap<String, List<String>>();
		HashMap<String, List<String>> masterMap = new HashMap<String, List<String>>();
		HashMap<String, List<String>> slaveMap = new HashMap<String, List<String>>();

		
		if(nodes==TargetScriptExecution.MASTER || nodes==TargetScriptExecution.ALL){
			result = cs.executeScriptOnNode(user, masterNode, instructions, privateKey, sudo, true);
			masterMap = (HashMap<String, List<String>>) result.getOutputMap();
		}
		if(nodes==TargetScriptExecution.SLAVE || nodes==TargetScriptExecution.ALL){
			for(ClusterNode node : slaveNodes){
				result = cs.executeScriptOnNode(user, node, instructions, privateKey, sudo, false);
				for(String s : result.getOutputMap().keySet()){
					slaveMap.put(s, result.getOutputMap().get(s));
				}
			}
		}
		if(nodes==TargetScriptExecution.MASTER)
			return masterMap;
		if(nodes==TargetScriptExecution.SLAVE)
			return slaveMap;
		for(String s : masterMap.keySet()){
			finalMap.put(s, masterMap.get(s));
		}
		for(String s : slaveMap.keySet()){
			finalMap.put(s, slaveMap.get(s));
		}
			
		return finalMap;
	}
	
}
