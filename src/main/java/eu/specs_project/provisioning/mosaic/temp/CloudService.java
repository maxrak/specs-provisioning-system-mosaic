package eu.specs_project.provisioning.mosaic.temp;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.jclouds.compute.config.ComputeServiceProperties.TIMEOUT_SCRIPT_COMPLETE;
import static org.jclouds.compute.options.TemplateOptions.Builder.overrideLoginCredentials;
import static org.jclouds.compute.predicates.NodePredicates.inGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.jclouds.ContextBuilder;
import org.jclouds.compute.ComputeService;
import org.jclouds.compute.ComputeServiceContext;
import org.jclouds.compute.RunNodesException;
import org.jclouds.compute.domain.ExecResponse;
import org.jclouds.compute.domain.NodeMetadata;
import org.jclouds.compute.domain.NodeMetadataBuilder;
import org.jclouds.compute.domain.Template;
import org.jclouds.compute.domain.TemplateBuilder;
import org.jclouds.domain.LoginCredentials;
import org.jclouds.scriptbuilder.ScriptBuilder;
import org.jclouds.scriptbuilder.statements.ssh.AuthorizeRSAPublicKeys;
import org.jclouds.scriptbuilder.statements.ssh.InstallRSAPrivateKey;
import org.jclouds.ssh.SshClient;
import org.jclouds.ssh.SshKeys;
import org.jclouds.ssh.jsch.config.JschSshClientModule;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Module;

import eu.specs_project.provisioning.mosaic.temp.ScriptExecutionResult.ScriptExecutionOutcome;

public class CloudService {
	private ComputeServiceContext context;
	private ComputeService compute;
	private String defaultUser;
	private Template template;

	
	
	public CloudService(String provider, String defaultUser, String accessKey, String secretKey){
		Properties properties = new Properties();
	    long scriptTimeout = TimeUnit.MILLISECONDS.convert(20, TimeUnit.MINUTES);
	    properties.setProperty(TIMEOUT_SCRIPT_COMPLETE, scriptTimeout + "");
	    this.defaultUser = defaultUser;
	    
	    
	    context = ContextBuilder.newBuilder(provider)
                .credentials(accessKey, secretKey)
                .overrides(properties)
                .modules(ImmutableSet.<Module> of(
                		 new JschSshClientModule()))
                .buildView(ComputeServiceContext.class); 
	    compute = context.getComputeService();
	}
	
	private Template obtainTemplate(String image, String hardwareId){
		TemplateBuilder templateBuilder = compute.templateBuilder();	
		template = templateBuilder
				.imageId(image)
				.hardwareId(hardwareId)
				.build();
		return template;
	}
		
	public NodesInfo createNodesInGroup(String groupName, int numberOfInstances, String image, String hwId) throws RunNodesException{
		Template template = obtainTemplate(image, hwId);
		Set<? extends NodeMetadata> nodes = compute.createNodesInGroup(groupName, numberOfInstances, template);
		NodesInfo info = new NodesInfo();
		
		Map <String, String> keys = SshKeys.generate();
		
		//info.privateKey = nodes.iterator().next().getCredentials().getPrivateKey().trim();
		info.privateKey= keys.get("private").trim();
		String publicK = keys.get("public"); 
		
		HashSet<String> publikSet = new HashSet<String>();
		publikSet.add(publicK);
		
		AuthorizeRSAPublicKeys authKeys = new AuthorizeRSAPublicKeys(publikSet);
		InstallRSAPrivateKey instPriv = new InstallRSAPrivateKey(info.privateKey);
		
		ScriptBuilder sb = new ScriptBuilder();

		sb.addStatement(authKeys);
		sb.addStatement(instPriv);
		ExecResponse er = null;
		for(NodeMetadata n : nodes){
			er = compute.runScriptOnNode(n.getId(), sb, 
				overrideLoginCredentials(getLoginForCommandExecution(defaultUser, n.getCredentials().credential.trim())).runAsRoot(false).wrapInInitScript(false));
			if(er.getExitStatus()!=0){
				//TODO
				//destroyClusterWithName(groupName);
				//throw new Exception("Error in configuration of custom RSA keys");
			}
		}
		
		
		for(NodeMetadata n : nodes){			
			info.nodes.add(new ClusterNode(n.getId(), n.getPublicAddresses().iterator().next(), n.getPrivateAddresses().iterator().next()));
		}
		return info;
	}
	
	public ClusterNode addNode(String clusterName) throws RunNodesException{
		NodeMetadata n = getOnlyElement(compute.createNodesInGroup(clusterName, 1, template));
		return new ClusterNode(n.getId(), n.getPublicAddresses().iterator().next(), n.getPrivateAddresses().iterator().next());
	}
	
	public ScriptExecutionResult addNewUserViaExec(String userName, String password, ClusterNode masterNode, List<ClusterNode> slaveNodes, String privateKeyDefaultUser){
		HashMap<String, String> keys = new HashMap<String, String>();
		String privateKeyNewUser, publicKeyNewUser;
		String userAddMaster[] = {
				"useradd "+userName,
				"mkdir /home/"+userName+"/.ssh",
				"touch /home/"+userName+"/.ssh/authorized_keys",
				"ssh-keygen -t rsa -f /home/"+userName+"/.ssh/id_rsa -q -N \'\'",
				"cat /home/"+defaultUser+"/.ssh/authorized_keys >> /home/"+userName+"/.ssh/authorized_keys",
				"cat /home/"+userName+"/.ssh/id_rsa.pub >> /home/"+userName+"/.ssh/authorized_keys",
				"chmod 600 /home/"+userName+"/.ssh/id_rsa",
				"chmod 644 /home/"+userName+"/.ssh/id_rsa.pub",
				"chmod 700 /home/"+userName+"/.ssh/",
				"chown -R "+userName+":"+userName+" /home/"+userName+"/.ssh"
		};
				
		SshClient ssh = getSshClientForNode(masterNode.getId(), defaultUser, privateKeyDefaultUser);
		ExecResponse er;
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		try{
			ssh.connect();
			for(String s : userAddMaster){
				er = executeSshCommand(ssh, "sudo bash -c \""+s+"\"");
				if(er.getExitStatus()!=0){
					scriptResult.setOutcome(ScriptExecutionOutcome.MASTER_ERROR);
					scriptResult.addAdditionalInfo("Error_Type", "User configuration on master node");
					return scriptResult;
				}
			}
			er = executeSshCommand(ssh, "sudo bash -c \"cat /home/"+userName+"/.ssh/id_rsa\"");
			privateKeyNewUser = er.getOutput().trim();
			keys.put("privateKeyNewUser", privateKeyNewUser);
			er = executeSshCommand(ssh, "sudo bash -c \"cat /home/"+userName+"/.ssh/id_rsa.pub\"");
			publicKeyNewUser = er.getOutput().trim();
			keys.put("publicKeyNewUser", publicKeyNewUser);
		}finally{
			if (ssh != null)
				ssh.disconnect();
		}
		scriptResult.setAdditionalInfo(keys);
		String userAddSlave[] = {
				"useradd "+userName,
				"mkdir /home/"+userName+"/.ssh",
				"touch /home/"+userName+"/.ssh/authorized_keys",
				"echo "+publicKeyNewUser+" >> /home/"+userName+"/.ssh/authorized_keys",
				"chmod 700 /home/"+userName+"/.ssh/",
				"chown -R "+userName+":"+userName+" /home/"+userName+"/.ssh"
		};
		
		for(ClusterNode n : slaveNodes){
			ssh = getSshClientForNode(n.getId(), defaultUser, privateKeyDefaultUser);
			try{
				ssh.connect();
				for(String s : userAddSlave){
					er = executeSshCommand(ssh, "sudo bash -c \""+s+"\"");
					if(er.getExitStatus()!=0){
						scriptResult.setOutcome(ScriptExecutionOutcome.SLAVE_ERROR);
						scriptResult.addAdditionalInfo("Error_Type", "User configuration on slave node/s");
						if(!scriptResult.getNodesWithError().contains(n.getId()))
							scriptResult.addNodeWithError(n);
					}
				}
			}finally{
				if (ssh != null)
					ssh.disconnect();
			}
		}
		return scriptResult;
	}
	
	public ScriptExecutionResult addNewUserViaExecForSingleNode(ClusterNode node, String userName, String password, String publicKeyNewUser, String privateKeyDefaultUser){
		String userAddSlave[] = {
				"useradd "+userName,
				"mkdir /home/"+userName+"/.ssh",
				"touch /home/"+userName+"/.ssh/authorized_keys",
				"echo "+publicKeyNewUser+" >> /home/"+userName+"/.ssh/authorized_keys",
				"chmod 700 /home/"+userName+"/.ssh/",
				"chown -R "+userName+":"+userName+" /home/"+userName+"/.ssh",
		};
		ExecResponse er;
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		SshClient ssh = getSshClientForNode(node.getId(), defaultUser, privateKeyDefaultUser);
		try{
			ssh.connect();
			for(String s : userAddSlave){
				er = executeSshCommand(ssh, "sudo bash -c \""+s+"\"");
				if(er.getExitStatus()!=0){
					scriptResult.setOutcome(ScriptExecutionOutcome.SLAVE_ERROR);
					scriptResult.addAdditionalInfo("Error_Type", "User configuration on slave node/s");
					if(!scriptResult.getNodesWithError().contains(node.getId()))
						scriptResult.addNodeWithError(node);
				}
			}
		}finally{
			if (ssh != null)
				ssh.disconnect();
		}
		return scriptResult;
	}
	
	
	public ScriptExecutionResult executeScriptOnNode(String user, ClusterNode node, String[] instructions, String privateKey, boolean sudo, boolean isMaster){
		ScriptExecutionResult scriptResult = new ScriptExecutionResult();
		ArrayList<String> output = new ArrayList<String>();
		SshClient ssh = getSshClientForNode(node.getId(), user, privateKey);
		ExecResponse er = null;
		try{
			ssh.connect();
			for(String s : instructions){
				if(sudo){
					er = executeSshCommand(ssh, "sudo bash -c \""+s+"\"");
				}
				else{
					er = executeSshCommand(ssh, s);
				}
				output.add(er.getOutput());
				if(er.getExitStatus()!=0){
					if(isMaster){
						scriptResult.setOutcome(ScriptExecutionOutcome.MASTER_ERROR);
						scriptResult.addAdditionalInfo("Error_Type", "MPI configuration on master node");
						scriptResult.addOutput(node.getId(), output);
						return scriptResult;
					}else{
						scriptResult.setOutcome(ScriptExecutionOutcome.SLAVE_ERROR);
						scriptResult.addAdditionalInfo("Error_Type", "MPI configuration on slave node/s");
						if(!scriptResult.getNodesWithError().contains(node.getId()))
							scriptResult.addNodeWithError(node);
					}
				}
			}
		}finally{
			if (ssh != null)
				ssh.disconnect();
		}
		scriptResult.addOutput(node.getId(), output);
		return scriptResult;
	}
	
	private SshClient getSshClientForNode(String nodeId, String user, String privateKey){
		NodeMetadata node = compute.getNodeMetadata(nodeId);
		return context.utils().sshForNode()
				.apply(NodeMetadataBuilder.fromNodeMetadata(node)
				.credentials(getLoginForCommandExecution(user, privateKey))
				.build());
	}
	
/*	public Set<? extends NodeMetadata> listNodesRunningInGroup(String groupName){
		Set<? extends NodeMetadata> nodesToList = (Set<? extends NodeMetadata>) compute.listNodes();
		for(NodeMetadata n : nodesToList){
			if(n.getStatus()==Status.RUNNING && n.getGroup().equalsIgnoreCase(groupName))
				;
			else
				nodesToList.remove(n);
		}
		return nodesToList;
	}
	*/
	public void suspendNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.suspendNode(hosts.get(i).getId());
		}
	}
	
	public void resumeNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.resumeNode(hosts.get(i).getId());
		}
	}

	public void rebootNodesInGroup(List<ClusterNode> hosts){
		for (int i = 0; i<hosts.size(); i++){
			compute.rebootNode(hosts.get(i).getId());
		}
	}
	
	public void destroyCluster(List<ClusterNode> hosts){ //distrugge anche keypair associati e il security group se non ci sono altre risorse pendenti associate
		for (int i = 0; i<hosts.size(); i++){
			compute.destroyNode(hosts.get(i).getId());
		}
	}
	
	public void destroyClusterWithName(String name){
		compute.destroyNodesMatching(inGroup(name));
	}
	
	public void destroySingleNode(ClusterNode node){
		compute.destroyNode(node.getId());
	}
	
	public String obtainMasterPrivateIP(String masterNodeId){
		return compute.getNodeMetadata(masterNodeId).getPrivateAddresses().iterator().next();
	}
	
//	private void generateKeys(){	
//		Map<String,String> keys = SshKeys.generate();
//		publicKeyNewUser = keys.get("public").trim();
//		privateKeyNewUser = keys.get("private").trim();
//	}
		
	private LoginCredentials getLoginForCommandExecution(String user, String credential) {
	
//	    return LoginCredentials.builder().
//	            user(user).privateKey(privateKey).build(); 
	    return LoginCredentials.builder().user(user).credential(credential).build();   
	}
	
	private ExecResponse executeSshCommand(SshClient ssh, String cmd){
		return ssh.exec(cmd);
	}	
	
	
	public class NodesInfo{
		public String privateKey;
		public List<ClusterNode> nodes = new ArrayList<ClusterNode>();
	}
	
	
}
