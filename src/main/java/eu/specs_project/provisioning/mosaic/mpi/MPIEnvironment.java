package eu.specs_project.provisioning.mosaic.mpi;

import java.util.UUID;

public class MPIEnvironment {
	
	private String build, vendor, version, referenceArchitecture, repositoryUrl, description, name;

	private final UUID id = UUID.randomUUID();
	
	public MPIEnvironment(String name, String build, String vendor, String version,
			String referenceArchitecture, String repositoryUrl,
			String description) {
		super();
		this.name = name;
		this.build = build;
		this.vendor = vendor;
		this.version = version;
		this.referenceArchitecture = referenceArchitecture;
		this.repositoryUrl = repositoryUrl;
		this.description = description;
	}

	public String getBuild() {
		return build;
	}

	public void setBuild(String build) {
		this.build = build;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getReferenceArchitecture() {
		return referenceArchitecture;
	}

	public void setReferenceArchitecture(String referenceArchitecture) {
		this.referenceArchitecture = referenceArchitecture;
	}

	public String getRepositoryUrl() {
		return repositoryUrl;
	}

	public void setRepositoryUrl(String repositoryUrl) {
		this.repositoryUrl = repositoryUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public UUID getId(){
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
