package eu.specs_project.provisioning.mosaic.cluster_entities;

import java.util.ArrayList;
import java.util.List;

public class SupportedProvider {
	
	private String name, jcloudsId, defaultUser;
	private List<HardwareType> hardwareTypes;
	
	public SupportedProvider(){
		hardwareTypes = new ArrayList<HardwareType>();
	}
	
	public SupportedProvider(String name, String jcloudsId, String defaultUser){
		this.name=name;
		this.jcloudsId=jcloudsId;
		this.defaultUser=defaultUser;
		hardwareTypes = new ArrayList<HardwareType>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJcloudsId() {
		return jcloudsId;
	}

	public void setJcloudsId(String jcloudsId) {
		this.jcloudsId = jcloudsId;
	}

	public String getDefaultUser() {
		return defaultUser;
	}

	public void setDefaultUser(String defaultUser) {
		this.defaultUser = defaultUser;
	}

	public List<HardwareType> getHardwareTypes() {
		return hardwareTypes;
	}

	public void setHardwareTypes(List<HardwareType> hardwareTypes) {
		this.hardwareTypes = hardwareTypes;
	}

	public void addHardwareType(HardwareType hwType){
		hardwareTypes.add(hwType);
	}
	
}
