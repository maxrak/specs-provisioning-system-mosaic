package eu.specs_project.provisioning.mosaic.cluster_entities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class Provider{
	

	private final UUID id = UUID.randomUUID();
	
	public Provider(){
		this.hardwareTypes=new ArrayList<UUID>();
		this.images=new ArrayList<VirtualMachineImage>();
	}
	
	public Provider(String name, String supportedProvider){
		this.supportedProvider=supportedProvider;
		this.hardwareTypes=new ArrayList<UUID>();
		this.images=new ArrayList<VirtualMachineImage>();
	}

	public String getSupportedProvider() {
		return supportedProvider;
	}

	public void setSupportedProvider(String supportedProvider) {
		this.supportedProvider = supportedProvider;
	}

	public List<UUID> getHardwareTypes() {
		return hardwareTypes;
	}

	public void setHardwaretypes(List<UUID> hardwareTypes) {
		this.hardwareTypes = hardwareTypes;
	}

	public List<VirtualMachineImage> getImages() {
		return images;
	}

	public void setImages(List<VirtualMachineImage> images) {
		this.images = images;
	}
	
	public void addHardwareType(UUID hardwareType){
		hardwareTypes.add(hardwareType);
	}

	public void addImage(VirtualMachineImage image){
		images.add(image);
	}
	
	public UUID getId(){
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setHardwareTypes(List<UUID> hardwareTypes) {
		this.hardwareTypes = hardwareTypes;
	}


	private String supportedProvider, name;
	private List <UUID> hardwareTypes;
	private List <VirtualMachineImage> images;
	
}
