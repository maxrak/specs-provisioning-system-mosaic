package eu.specs_project.provisioning.mosaic.failure_recovery;

import eu.specs_project.provisioning.mosaic.temp.CloudService;
import eu.specs_project.provisioning.mosaic.temp.ClusterNode;
import eu.specs_project.provisioning.mosaic.temp.ScriptExecutionResult;

public class NullRecoveryPolicy extends RecoveryPolicy {

	public NullRecoveryPolicy(){
		super();
	}
	
	@Override
	public RecoveryResult recovery(CloudService cs, ClusterNode node, ScriptExecutionResult result) {
		
		return null;
	}
}
