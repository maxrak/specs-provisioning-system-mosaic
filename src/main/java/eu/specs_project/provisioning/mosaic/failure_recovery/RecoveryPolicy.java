package eu.specs_project.provisioning.mosaic.failure_recovery;

import java.util.HashMap;
import java.util.Map;

import eu.specs_project.provisioning.mosaic.temp.CloudService;
import eu.specs_project.provisioning.mosaic.temp.ClusterNode;
import eu.specs_project.provisioning.mosaic.temp.ScriptExecutionResult;

public abstract class RecoveryPolicy {
	
	protected Map<String, String> parameters;
	
	public RecoveryPolicy(){
		parameters = new HashMap<String, String>();
	}
	
	public RecoveryResult recovery(CloudService cs, ClusterNode node, ScriptExecutionResult result){
		return new RecoveryResult();
	}
	
	public void setParameters(Map<String, String> parameters){
		this.parameters=parameters;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}
	
}
