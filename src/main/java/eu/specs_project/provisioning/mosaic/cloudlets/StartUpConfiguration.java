package eu.specs_project.provisioning.mosaic.cloudlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import eu.mosaic_cloud.cloudlets.connectors.kvstore.IKvStoreConnector;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.IKvStoreConnectorFactory;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackArguments;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.core.ICallback;
import eu.mosaic_cloud.cloudlets.core.ICloudletController;
import eu.mosaic_cloud.cloudlets.tools.DefaultCloudletCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultKvStoreConnectorCallback;
import eu.mosaic_cloud.platform.core.configuration.ConfigUtils;
import eu.mosaic_cloud.platform.core.configuration.ConfigurationIdentifier;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.platform.core.utils.JsonDataEncoder;
import eu.mosaic_cloud.platform.core.utils.PlainTextDataEncoder;
import eu.mosaic_cloud.tools.callbacks.core.CallbackCompletion;
import eu.specs_project.provisioning.mosaic.cluster_entities.EndUser;
import eu.specs_project.provisioning.mosaic.cluster_entities.Provider;
import eu.specs_project.provisioning.mosaic.cluster_entities.SupportedProvider;
import eu.specs_project.provisioning.mosaic.cluster_entities.UserCSPAccount;
import eu.specs_project.provisioning.mosaic.cluster_entities.VirtualMachineImage;
import eu.specs_project.provisioning.mosaic.mpi.MPIEnvironment;
import eu.specs_project.provisioning.mosaic.utilities.ConfigurationParameters;
import eu.specs_project.provisioning.mosaic.utilities.ProvidersParser;

public class StartUpConfiguration {
	
	public static final class StartUpContext{
		ICloudletController<StartUpContext> cloudlet;
		IConfiguration configuration;
		IKvStoreConnector<EndUser, Void> usersBucket;
		IKvStoreConnector<Provider, Void> providersBucket;
		IKvStoreConnector<MPIEnvironment, Void> mpiEnvsBucket;
		IKvStoreConnector<SupportedProvider, Void> supportedProvidersBucket;
		IKvStoreConnector<String, Void> htmlBucket;
		String configFile;
		public String awspass;
		public String rackpass;
	}
	
	
	public static final class LifeCycleHandler	extends DefaultCloudletCallback<StartUpContext>{
		
		public CallbackCompletion<Void> initialize (final StartUpContext context, final CloudletCallbackArguments<StartUpContext> arguments){
			context.cloudlet = arguments.getCloudlet ();
			final IConfiguration configuration = context.cloudlet.getConfiguration();
			context.configFile = ConfigUtils.resolveParameter(configuration, "configuration.file.name", String.class, "configuration.properties");
			final IConfiguration usersBucketConfiguration = configuration.spliceConfiguration (ConfigurationIdentifier.resolveAbsolute ("users.bucket"));
			final IConfiguration providersBucketConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("providers.bucket"));
			final IConfiguration mpiEnvsConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("mpi.envs.bucket"));
			final IConfiguration htmlConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("html.bucket"));
			final IConfiguration supportedProvidersBucketConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("supported.providers.bucket"));
			this.logger.info("initializing start up components...");
			context.usersBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(usersBucketConfiguration,
					EndUser.class, JsonDataEncoder.create(EndUser.class), new DefaultKvStoreConnectorCallback<StartUpContext, EndUser, Void>(), context);
			context.providersBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(providersBucketConfiguration,
					Provider.class, JsonDataEncoder.create(Provider.class), new DefaultKvStoreConnectorCallback<StartUpContext, Provider, Void>(), context);
			context.mpiEnvsBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(mpiEnvsConfiguration,
					MPIEnvironment.class, JsonDataEncoder.create(MPIEnvironment.class), new DefaultKvStoreConnectorCallback<StartUpContext, MPIEnvironment, Void>(), context);
			context.supportedProvidersBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(supportedProvidersBucketConfiguration,
					SupportedProvider.class, JsonDataEncoder.create(SupportedProvider.class), new DefaultKvStoreConnectorCallback<StartUpContext, SupportedProvider, Void>(), context);
			context.htmlBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(htmlConfiguration,
					String.class, PlainTextDataEncoder.create(), new DefaultKvStoreConnectorCallback<StartUpContext, String, Void>(), context);
			return CallbackCompletion.createAndChained(context.usersBucket.initialize(), context.providersBucket.initialize(), context.mpiEnvsBucket.initialize(), context.supportedProvidersBucket.initialize(), context.htmlBucket.initialize());
		}
		
		public CallbackCompletion<Void> initializeSucceeded(StartUpContext context,
				CloudletCallbackCompletionArguments<StartUpContext> arguments){	
			
			this.logger.info("loading data in kv store....");
			try {
				Initializer.startUpConfig(context);
				Initializer.loadStubData(context);
			} catch (IOException e1) {

				e1.printStackTrace();
			}			
			
			this.logger.info("loading data complete!");
						
			this.logger.info("start up cloudlet initialized successfully");
			
			return (ICallback.SUCCESS);
		}
		
		public CallbackCompletion<Void> destroySucceeded(StartUpContext context,
				CloudletCallbackCompletionArguments<StartUpContext> arguments){
			this.logger.info("cloudlet destroyed successfully");
			return (ICallback.SUCCESS);
		}
		
		public CallbackCompletion<Void> destroy (final StartUpContext context,
				final CloudletCallbackArguments<StartUpContext> arguments){
			this.logger.info ("destroying cloudlet and components...");
			return CallbackCompletion.createAndChained(context.usersBucket.destroy(), context.providersBucket.destroy(), context.mpiEnvsBucket.destroy(), context.supportedProvidersBucket.destroy(), context.htmlBucket.destroy());
		}
	}
	
	public static class Initializer {
		
		private static List<SupportedProvider> sp;
		
		

		public static void startUpConfig(StartUpContext context) throws IOException {
			
			Properties configProp = new Properties();
			configProp.load(context.getClass().getClassLoader().getResourceAsStream(context.configFile));		
			String configPath = configProp.getProperty("supported_providers_config");
			ConfigurationParameters.setParameter("max_cluster_nodes", configProp.getProperty("max_cluster_nodes"));
			ConfigurationParameters.setParameter("mpi_user_username", configProp.getProperty("mpi_user_username"));
			context.awspass=configProp.getProperty("cerict_aws_pass");
			context.rackpass=configProp.getProperty("cerict_rack_pass");
			
			ProvidersParser parser = new ProvidersParser(context.getClass().getClassLoader().getResourceAsStream(configPath));
			sp = parser.parse();
			int i = 1;
			for(SupportedProvider p : sp){
				context.supportedProvidersBucket.set(String.valueOf(i), p);
				i++;
			}
			
			String content = "";
			
		    try {
		        BufferedReader in = new BufferedReader(new InputStreamReader(context.getClass().getClassLoader().getResourceAsStream("html/index.html")));
		        String str;
		        while ((str = in.readLine()) != null) {
		            content +=str+"\n";
		        }
		        in.close();
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
			context.htmlBucket.set("index", content);
		}
		
		
		public static void  loadStubData (StartUpContext context) throws IOException {
						
			MPIEnvironment mpi1 = new MPIEnvironment("openmpi_1.6.5_32bit_built_on_redhat", "1.6.5", "openmpi","redhat_6.4" ,"x86/32",  "https://s3.amazonaws.com/mauro_mpi/ompi_1.6.5_32_redhat.tar.gz", "");
			MPIEnvironment mpi2 = new MPIEnvironment("openmpi_1.6.5_64bit_built_on_centos", "1.6.5", "openmpi","centos_6.5", "x86/64",  "https://s3.amazonaws.com/mauro_mpi/ompi_1.6.5_64_centos.tar.gz", "");	
			MPIEnvironment mpi3 = new MPIEnvironment("openmpi_1.6.5_64bit_built_on_redhat", "1.6.5", "openmpi","redhat_6.5", "x86/64",  "https://s3.amazonaws.com/mauro_mpi/ompi_1.6.5_64_redhat.tar.gz", "");
			MPIEnvironment mpi4 = new MPIEnvironment("openmpi_1.6.8_64bit_built_on_centos", "1.6.8", "openmpi","centos_6.5", "x86/64",  "https://s3.amazonaws.com/mauro_mpi/ompi_1.6.8_64_centos.tar.gz", "");
			
			
			context.mpiEnvsBucket.set(mpi1.getId().toString(), mpi1);
			context.mpiEnvsBucket.set(mpi2.getId().toString(), mpi2);
			context.mpiEnvsBucket.set(mpi3.getId().toString(), mpi3);
			context.mpiEnvsBucket.set(mpi4.getId().toString(), mpi4);

			
			List <UUID> envsList = new ArrayList<UUID>();
			envsList.add(mpi1.getId());
 
			
			List <VirtualMachineImage> images = new ArrayList<VirtualMachineImage>();
			VirtualMachineImage img1 = new VirtualMachineImage("red_hat_6.4_32bit", "us-east-1/ami-7e175617");
			img1.setMPIEnvs(envsList);
			
			VirtualMachineImage img2 = new VirtualMachineImage("linux_mint_stub", "afoub-sb8919");
			envsList = new ArrayList<UUID>();
			envsList.add(mpi2.getId());
			envsList.add(mpi3.getId());
			img2.setMPIEnvs(envsList);
			
			
			VirtualMachineImage img3 = new VirtualMachineImage("red_hat_6.5_64bit", "LON/8a10a510-ec4c-43db-8b87-6a58438b3f19");
			VirtualMachineImage img4 = new VirtualMachineImage("centos_6.5_64bit", "LON/70d38a32-5f63-45df-a0e7-7e06fc89370a");
			envsList = new ArrayList<UUID>();
			envsList.add(mpi2.getId());
			envsList.add(mpi3.getId());
			envsList.add(mpi4.getId());
			img3.setMPIEnvs(envsList);
			img4.setMPIEnvs(envsList);
			
			Provider p1 = new Provider("AWS", "1");
			p1.addHardwareType(sp.get(0).getHardwareTypes().get(0).getId());
			p1.addHardwareType(sp.get(0).getHardwareTypes().get(1).getId());
			images.add(img1);
			//images.add(img2);
			p1.setImages(images);
			
//			Provider p2 = new Provider("AWS", "1");
//			p2.addHardwareType(sp.get(0).getHardwareTypes().get(1).getId());
//			images = new ArrayList<VirtualMachineImage>();
//			images.add(img1);
//			images.add(img2);
//			p2.setImages(images);
			
			Provider p3 = new Provider("RACKSPACE", "2");
			p3.addHardwareType(sp.get(1).getHardwareTypes().get(0).getId());
			p3.addHardwareType(sp.get(1).getHardwareTypes().get(1).getId());
			p3.addHardwareType(sp.get(1).getHardwareTypes().get(2).getId());
			images = new ArrayList<VirtualMachineImage>();
			images.add(img3);
			images.add(img4);
			p3.setImages(images);
			
			context.providersBucket.set(p1.getId().toString(),p1);
//			context.providersBucket.set(p2.getId().toString(),p2);
			context.providersBucket.set(p3.getId().toString(),p3);
			
			 
			UserCSPAccount aws_cerict = new UserCSPAccount(p1.getId(), "AWS_cerict");
			UserCSPAccount rack_cerict = new UserCSPAccount(p3.getId(), "RACKSPACE_UK_cerict");
		 
			
 
			
			//cerict
			aws_cerict.setProviderUsername("AKIAII4Z2JZQAMIJLUXQ");
			aws_cerict.setProviderPassword(context.awspass);
			
			rack_cerict.setProviderUsername("specscerictuk");
			rack_cerict.setProviderPassword(context.rackpass);
			
 
			
			
			EndUser u1 = new EndUser();
			u1.setUsername("Cerict SPECS end user");
			ArrayList <UserCSPAccount> listacc = new ArrayList<UserCSPAccount>();
			listacc.add(aws_cerict);
			listacc.add(rack_cerict);
			u1.setAccounts(listacc);
			
 
			
			context.usersBucket.set(u1.getId().toString(), u1);
 
		}
	}
}
