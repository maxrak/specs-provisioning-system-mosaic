package eu.specs_project.provisioning.mosaic.cloudlets;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.Callable;

import org.slf4j.Logger;

import com.google.gson.Gson;

import eu.mosaic_cloud.cloudlets.connectors.executors.ExecutionFailedCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.executors.ExecutionSucceededCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.executors.IExecutor;
import eu.mosaic_cloud.cloudlets.connectors.executors.IExecutorFactory;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.IKvStoreConnector;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.IKvStoreConnectorFactory;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.KvStoreCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.AmqpQueueConsumeCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueueConsumerConnector;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueueConsumerConnectorFactory;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueuePublisherConnector;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueuePublisherConnectorFactory;
import eu.mosaic_cloud.cloudlets.core.CallbackArguments;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackArguments;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.core.GenericCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.core.ICallback;
import eu.mosaic_cloud.cloudlets.core.ICloudletController;
import eu.mosaic_cloud.cloudlets.tools.DefaultAmqpPublisherConnectorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultAmqpQueueConsumerConnectorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultCloudletCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultExecutorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultKvStoreConnectorCallback;
import eu.mosaic_cloud.connectors.queue.amqp.IAmqpMessageToken;
import eu.mosaic_cloud.platform.core.configuration.ConfigurationIdentifier;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.platform.core.utils.JsonDataEncoder;
import eu.mosaic_cloud.platform.core.utils.PlainTextDataEncoder;
import eu.mosaic_cloud.tools.callbacks.core.CallbackCompletion;
import eu.specs_project.provisioning.mosaic.temp.Cluster;
import eu.specs_project.provisioning.mosaic.temp.ConfigStatus;
import eu.specs_project.provisioning.mosaic.temp.Cluster.State;

public class ManageClusterCloudlet{


	public static final class ManageContext{
		ICloudletController<ManageContext> cloudlet;
		IConfiguration configuration;
		IExecutor<Boolean, UUID> createClusterExecutor;
		IExecutor<Void, UUID> destroyClusterExecutor;
		IKvStoreConnector<String, String> clustersBucket;
		IAmqpQueueConsumerConnector<HashMap, UUID> createClusterConsumer;
		IAmqpQueueConsumerConnector<HashMap, Void> destroyClusterConsumer;
		IAmqpQueuePublisherConnector<ConfigStatus, Void> createAckPublisher;
		HashMap<UUID, IAmqpMessageToken> createClusterToken;
		HashMap<UUID, IAmqpMessageToken> destroyClusterToken;
		HashMap<UUID, HashMap<String, String>> clusterAttributes;
		
	}
	
	public static final class ClusterKvStoreCallback extends
		DefaultKvStoreConnectorCallback<ManageContext, String, String>{

		@Override
		public CallbackCompletion<Void> getSucceeded(ManageContext context,
				KvStoreCallbackCompletionArguments<String, String> arguments) {
			this.logger.info("cluster get succeeded...");
			Cluster c = new Gson().fromJson(arguments.getValue(), Cluster.class);
			String command = arguments.getExtra();
			if(command.equalsIgnoreCase("destroy")){
				if(c.getClusterState()==State.ERROR || c.getClusterState()==State.READY){
					context.destroyClusterExecutor.execute(new DestroyClusterExecutor(context, c, this.logger), c.getId());
				}
			}
			else if(command.equalsIgnoreCase("start")){
				if(c.getClusterState()==State.STOPPED){
					
				}
			}
			else if(command.equalsIgnoreCase("stop")){
				if(c.getClusterState()==State.READY){
					
				}
			}
			return ICallback.SUCCESS;
		}
	
	
	}
	
	public static final class CreateClusterAmqpConsumerCallback extends
		DefaultAmqpQueueConsumerConnectorCallback<ManageContext, HashMap, UUID> {

		public CallbackCompletion<Void> initializeSucceeded(final ManageContext context, final CallbackArguments arguments){
			this.logger.info("create cluster queue consumer connector initialized successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> destroySucceeded(final ManageContext context, final CallbackArguments arguments){
			this.logger.info("create cluster queue consumer connector destroyed successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> consume(final ManageContext context, final AmqpQueueConsumeCallbackArguments<HashMap> arguments){
			this.logger.info("Manage cluster cloudlet: received create message from frontend...");
			final HashMap<String, String> attributes = arguments.getMessage();
			UUID id = UUID.fromString(attributes.get("id"));
			context.clusterAttributes.put(id, attributes);
			context.createClusterToken.put(id, arguments.getToken());
			context.createClusterExecutor.execute(new CreateClusterExecutor(context, attributes, this.logger), id);
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> acknowledgeSucceeded(final ManageContext context, final GenericCallbackCompletionArguments<UUID> arguments){
			this.logger.info("create cluster message acknowledge succeeded...");
			return ICallback.SUCCESS;
		}

	}

	
	public static class CreateClusterExecutor implements Callable<Boolean>{
		
		private Logger logger;
		private ManageContext context;
		private HashMap<String, String> input;
		
		public CreateClusterExecutor(final ManageContext context, HashMap<String, String> input, Logger logger){
			super ();
			this.context = context;
			this.input = input;
			this.logger = logger;
			this.logger.info("create cluster executor created...");
		}
		
		public Boolean call() {
			this.logger.info("create cluster execution in progress...");
			//TODO verificare num max nodi
			Cluster c = new Cluster(input.get("id"), input.get("clusterName"), input.get("provider"), input.get("defaultUser"),
					Integer.parseInt(input.get("nodes")), input.get("cspUsername"), input.get("cspPassword"),
					input.get("clusterDescription"), input.get("image"), input.get("providerHwId"), input.get("user"));	
			c.setErrorDescription("---");
			context.clustersBucket.set(c.getId().toString(), new Gson().toJson(c, Cluster.class));
			try{
				c.createNodesInCluster();
				c.setErrorDescription("No Errors");
				c.setClusterState(State.READY);
				context.clustersBucket.set(c.getId().toString(), new Gson().toJson(c, Cluster.class));
				this.logger.info("cluster created successfully");
				this.logger.info("default user key: \n" +c.getPrivateKeyDefaultUser());
			} catch(NoSuchElementException e){
				c.setClusterState(State.ERROR);
				c.setErrorDescription("Network Error. \nThe cluster has not been created");
				context.clustersBucket.set(c.getId().toString(), new Gson().toJson(c, Cluster.class));
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				this.logger.info("******create cluster execution failed****** reason:" +sw.toString());
				return false;
			} catch (Exception e) {
				this.logger.info("impossible to start nodes as a cluster... --> " + e.getMessage());
				c.setClusterState(State.ERROR);
				c.setErrorDescription("Creation Error");
				context.clustersBucket.set(c.getId().toString(), new Gson().toJson(c, Cluster.class));
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				this.logger.info("******create cluster execution failed****** reason:" +sw.toString());
				return false;
			}
			return true;
		}		
	}
	
	public static final class CreateClusterExecutorCallback extends DefaultExecutorCallback<ManageContext, Boolean, UUID>{

		@Override
		public CallbackCompletion<Void> initializeSucceeded(ManageContext context,
				CallbackArguments arguments) {

			this.logger.info("create cluster executor initialized successfully");
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> executionSucceeded(ManageContext context,
				ExecutionSucceededCallbackArguments<Boolean, UUID> arguments) {

			this.logger.info("create cluster execution succeeded");
			UUID id = arguments.getExtra();
			context.createClusterConsumer.acknowledge(context.createClusterToken.get(id), id);
			context.createAckPublisher.publish(new ConfigStatus(id, arguments.getOutcome()));
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> executionFailed(ManageContext context,
				ExecutionFailedCallbackArguments<UUID> arguments) {

			this.logger.info("********create cluster execution failed********: "+arguments.getException().getMessage());
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			arguments.getException().printStackTrace(pw);
			this.logger.info("******create cluster execution failed****** reason:" +sw.toString());
			UUID id = arguments.getExtra();
			context.createClusterConsumer.acknowledge(context.createClusterToken.get(id), id);
			context.createAckPublisher.publish(new ConfigStatus(id, false));
			return ICallback.SUCCESS;
		}
	}
	
	public static final class DestroyClusterAmqpConsumerCallback extends
			DefaultAmqpQueueConsumerConnectorCallback<ManageContext, HashMap, Void> {

		public CallbackCompletion<Void> initializeSucceeded(final ManageContext context, final CallbackArguments arguments){
			this.logger.info("destroy cluster queue consumer connector initialized successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> destroySucceeded(final ManageContext context, final CallbackArguments arguments){
			this.logger.info("destroy cluster queue consumer connector destroyed successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> consume(final ManageContext context, final AmqpQueueConsumeCallbackArguments<HashMap> arguments){
			this.logger.info("Manage cluster cloudlet: received destroy message from frontend...");
			final HashMap<String, String> attributes = arguments.getMessage();
			UUID id = UUID.fromString(attributes.get("id"));
			context.destroyClusterToken.put(id, arguments.getToken());
			context.clustersBucket.get(id.toString(), "destroy");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> acknowledgeSucceeded(final ManageContext context, final GenericCallbackCompletionArguments<Void> arguments){
			this.logger.info("create cluster message acknowledge succeeded...");
			return ICallback.SUCCESS;
		}

	}
	
	
	public static class DestroyClusterExecutor implements Callable<Void>{
		
		private Logger logger;
		private ManageContext context;
		private Cluster c;
		
		public DestroyClusterExecutor(final ManageContext context, Cluster c, Logger logger){
			super ();
			this.context = context;
			this.c = c;
			this.logger = logger;
			this.logger.info("destroy cluster executor created...");
		}
		
		public Void call() {
			this.logger.info("destroy cluster execution in progress...");
			c.destroyCluster();
			return null;
		}		
	}
	
	public static final class DestroyClusterExecutorCallback extends DefaultExecutorCallback<ManageContext, Void, UUID>{

		@Override
		public CallbackCompletion<Void> initializeSucceeded(ManageContext context,
				CallbackArguments arguments) {

			this.logger.info("destroy cluster executor initialized successfully");
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> executionSucceeded(ManageContext context,
				ExecutionSucceededCallbackArguments<Void, UUID> arguments) {

			this.logger.info("destroy cluster execution succeeded");
			UUID id = arguments.getExtra();
			context.destroyClusterConsumer.acknowledge(context.destroyClusterToken.get(id), null);
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> executionFailed(ManageContext context,
				ExecutionFailedCallbackArguments<UUID> arguments) {

			this.logger.info("********destroy cluster execution failed********: "+arguments.getException().getMessage());
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			arguments.getException().printStackTrace(pw);
			this.logger.info("******destroy cluster execution failed****** reason:" +sw.toString());
			
			return ICallback.SUCCESS;
		}
	}
	
	
	public static final class CreateAckPublisherCallback extends
		DefaultAmqpPublisherConnectorCallback<ManageContext, UUID, Void> {
		
		@Override
		public CallbackCompletion<Void> initializeSucceeded(
				ManageContext context, CallbackArguments arguments) {
			this.logger.info("create ack publisher queue initialized successfully");
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> publishSucceeded(ManageContext context,
				GenericCallbackCompletionArguments<Void> arguments) {
			this.logger.info("create ack publish succeeded");
			return ICallback.SUCCESS;
		}
		
	}
	
	
	
	
	public static final class LifeCycleHandler
		extends DefaultCloudletCallback<ManageContext>{

		public CallbackCompletion<Void> destroy (final ManageContext context, final CloudletCallbackArguments<ManageContext> arguments){
			return CallbackCompletion.createAndChained(context.clustersBucket.destroy(), context.createClusterConsumer.destroy(),
					context.createAckPublisher.destroy(), context.createClusterExecutor.destroy(), context.destroyClusterConsumer.destroy(),
					context.destroyClusterExecutor.destroy());
		}
		
	
		public CallbackCompletion<Void> initialize (final ManageContext context, final CloudletCallbackArguments<ManageContext> arguments){
			context.cloudlet = arguments.getCloudlet ();
			final IConfiguration configuration = context.cloudlet.getConfiguration();
			final IConfiguration createClusterConsumerConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("create.consumer"));
			final IConfiguration createAckPublisherConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("create.ack.publisher"));
			final IConfiguration destroyClusterConsumerConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("destroy.consumer"));
			final IConfiguration createClusterExecutorConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("executor"));
			final IConfiguration destroyClusterExecutorConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("executor"));
			final IConfiguration clustersBucketConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("clusters.bucket"));
			context.createClusterToken = new HashMap<UUID, IAmqpMessageToken>();
			context.clusterAttributes = new HashMap<UUID, HashMap<String, String>>();
			this.logger.info("initializing manage cluster cloudlet components...");			
			context.createClusterConsumer = context.cloudlet.getConnectorFactory(IAmqpQueueConsumerConnectorFactory.class).create(createClusterConsumerConfiguration,
					HashMap.class, JsonDataEncoder.create(HashMap.class), new CreateClusterAmqpConsumerCallback(), context);
			context.destroyClusterConsumer = context.cloudlet.getConnectorFactory(IAmqpQueueConsumerConnectorFactory.class).create(destroyClusterConsumerConfiguration,
					HashMap.class, JsonDataEncoder.create(HashMap.class), new DestroyClusterAmqpConsumerCallback(), context);
			context.createAckPublisher = context.cloudlet.getConnectorFactory(IAmqpQueuePublisherConnectorFactory.class).create(createAckPublisherConfiguration,
					ConfigStatus.class, JsonDataEncoder.create(ConfigStatus.class), new DefaultAmqpPublisherConnectorCallback<ManageContext, ConfigStatus, Void>(), context);
			context.createClusterExecutor = context.cloudlet.getConnectorFactory(IExecutorFactory.class).create(createClusterExecutorConfiguration,
					new CreateClusterExecutorCallback(), context);
			context.destroyClusterExecutor = context.cloudlet.getConnectorFactory(IExecutorFactory.class).create(destroyClusterExecutorConfiguration,
					new DestroyClusterExecutorCallback(), context);
			context.clustersBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(clustersBucketConfiguration,
					String.class, PlainTextDataEncoder.create(), new DefaultKvStoreConnectorCallback<ManageContext, String, String>(), context);
			return CallbackCompletion.createAndChained(context.clustersBucket.initialize(), context.createClusterConsumer.initialize(),
					context.createAckPublisher.initialize(), context.destroyClusterConsumer.initialize(), context.createClusterExecutor.initialize(),
					context.destroyClusterExecutor.initialize());
		}
	
		public CallbackCompletion<Void> initializeSucceeded(ManageContext context,
				CloudletCallbackCompletionArguments<ManageContext> arguments){
			this.logger.info("manage cluster cloudlet initialized successfully");
			return (ICallback.SUCCESS);
		}
		
		public CallbackCompletion<Void> destroySucceeded(ManageContext context,
				CloudletCallbackCompletionArguments<ManageContext> arguments){
			this.logger.info("manage cluster cloudlet destroyed successfully");
			return (ICallback.SUCCESS);
		}
	}
		
}
