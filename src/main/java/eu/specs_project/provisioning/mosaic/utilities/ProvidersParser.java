package eu.specs_project.provisioning.mosaic.utilities;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.specs_project.provisioning.mosaic.cluster_entities.HardwareType;
import eu.specs_project.provisioning.mosaic.cluster_entities.SupportedProvider;

public class ProvidersParser {

	private Document doc;
	
	public ProvidersParser(String fileNameToParse){		
		try {
			File providers = new File(fileNameToParse);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(providers);
			doc.getDocumentElement().normalize();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public ProvidersParser(InputStream is){
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public List<SupportedProvider> parse(){
		NodeList nodes = doc.getElementsByTagName("provider");
		ArrayList<SupportedProvider> supportedProviders = new ArrayList<SupportedProvider>();
		SupportedProvider sp = null;
		String name, jcloudsId, defaultUser, providerHwId, provName;
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				name = getAttrValueOfNode("name", element);
				jcloudsId = getValue("jcloudsId", element);
				defaultUser = getValue("defaultUser", element);
				sp = new SupportedProvider(name, jcloudsId, defaultUser);
				NodeList leafNodes = doc.getElementsByTagName("hardwareDescriptions").item(i).getChildNodes();
				for (int j = 0; j < leafNodes.getLength(); j++) {
					Node leafNode = leafNodes.item(j);
					HardwareType ht = null;
					if (leafNode.getNodeType() == Node.ELEMENT_NODE) {
						Element leafElement = (Element) leafNode;
							providerHwId = getAttrValueOfNode("providerHwId", leafElement);
							provName = getAttrValueOfNode("name", leafElement);
							ht = new HardwareType(providerHwId, provName);
							sp.addHardwareType(ht);
					}
				}
				supportedProviders.add(sp);
			}
		}
		return supportedProviders;
	}
	
	private static String getValue(String tag, Element element) {
		NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodes.item(0);
		return node.getNodeValue();
	}


	private static String getAttrValueOfNode(String attrTag, Element element) {
		NamedNodeMap nodes = element.getAttributes();
		return nodes.getNamedItem(attrTag).getNodeValue();
	}
	
/*	private static String getAttrValue(String nodeTag, String attrTag, Element element) {
		NamedNodeMap nodes = element.getElementsByTagName(nodeTag).item(0).getAttributes();
		return nodes.getNamedItem(attrTag).getNodeValue();
	}*/
	
}
