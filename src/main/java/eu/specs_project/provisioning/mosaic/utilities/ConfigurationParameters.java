package eu.specs_project.provisioning.mosaic.utilities;

import java.util.HashMap;

public class ConfigurationParameters {
	
	public static String getValueByKey(String key){
		return map.get(key);
	}
	
	public static void setParameter(String key, String value){
		map.put(key, value);
	}
	
	private static HashMap<String, String> map = new HashMap<String, String>();
}
